## Project Expectations

You will need to do the exercise completely individually.

**Steps need to be followed**

– *Responsive Design*

 - All CSS development will be done through SASS, and will be built
   manually 	using sass compiler
   (add configuration of sass in webpack)
 - Follow BEM for css class creation

*- Coding & Development*

 ***– **Components Creation -***** 

 - Components should be created using react hooks. 
 - Do not put everything inside a single component. Components should be concise and should
   express itself.
 - Follow Atomic design pattern  
 - Code should be well optimized
 - Use Context API for state management if props drilling is required
 
 ***– *Linting & formatting****
	Linting can be done using eslint and formatting should be done using prettier.
	
 ***– *Tool used for coding****
VSCode should be used for coding. Eslint and prettier plugins are already present for VSCode. Install those as well before you start writing your code.

***– Build process***
Using webpack, it should be enhanced progressively. In addition should have sass and JS build jobs.

– ***Unit Testing***

 - Test case coverage should be over 90%. Test cases should be using
   react-testing-library. 
   
 - Test cases should be written for each and    every functionality you
   create .

***– Node JS API creation***

 - Create APIs in node js,
 - You can use any framework Express, Hapi or
   Fastify.
 - Graphql layer can be added if time permits  

**GIT Strategy will be as below:**

o Fork following repository - [https://github.com/gautam-in/shopping-cart-assignment](https://github.com/gautam-in/shopping-cart-assignment)

o Next you need to create a branch called develop, where you will commit code.

o At the end of each week, raise a PR to master branch.

o Each PR will be reviewed by mentor and review comments needs to be incorporated.


## Boilerplate Details

# react-webpack-5-boilerplate

React 17 Boilerplate with React 17, Webpack 5, using babel, Jest unit testing, sass, with a hot dev server and an optimized production build

### Development server

```bash
yarn start / npm start
```

You can view the development server at `localhost:8080`.
(change port in ./config/webpack.dev.js)

### Unit Test

```bash
 yarn test / npm test
```

### Production build

```bash
 yarn build / npm run build
```

### Component scaffolding: 
Consistently generate components using Plop.
```bash
 yarn generate / npm run generate
```

### storybook: 
Sample code is added to 'src/stories'

```bash
  npm run storybook
```


## Features

- [React 17](https://reactjs.org/)
- [Webpack 5](https://webpack.js.org/)
- [Babel](https://babeljs.io/)
- [Jest 26](http://jestjs.io/)
- [PostCss](https://postcss.org/)  
- [Sass](https://sass-lang.com/)
- [Husky](https://github.com/typicode/husky)
- [StoryBook](https://storybook.js.org/)
- [Generators](https://www.npmjs.com/package/plop)
- [Eslint](https://eslint.org/)

## Dependencies

### webpack

- [`webpack`](https://github.com/webpack/webpack) - Module and asset bundler.
- [`webpack-cli`](https://github.com/webpack/webpack-cli) - Command line interface for webpack
- [`webpack-dev-server`](https://github.com/webpack/webpack-dev-server) - Development server for webpack
- [`webpack-merge`](https://github.com/survivejs/webpack-merge) - Simplify development/production configuration

### Loaders

- [`babel-loader`](https://webpack.js.org/loaders/babel-loader/) - Transpile files with Babel and webpack
- [`sass-loader`](https://webpack.js.org/loaders/sass-loader/) - Load SCSS and compile to CSS
- [`node-sass`](https://github.com/sass/node-sass) - Node Sass
- [`css-loader`](https://webpack.js.org/loaders/css-loader/) - Resolve CSS imports
- [`postcss-loader`](https://webpack.js.org/loaders/postcss-loader/) - Loader to process CSS with PostCSS
- [`style-loader`](https://webpack.js.org/loaders/style-loader/) - Inject CSS into the DOM

### Babel

- [`@babel/core`](https://www.npmjs.com/package/@babel/core) - Transpile ES6+ to backwards compatible JavaScript
- [`@babel/plugin-proposal-class-properties`](https://babeljs.io/docs/en/babel-plugin-proposal-class-properties) - Use properties directly on a class (an example Babel config)
- [`@babel/preset-env`](https://babeljs.io/docs/en/babel-preset-env) - Smart defaults for Babel

### Jest

- [`jest`](https://jestjs.io/) - Delightful JavaScript Testing
- [`@testing-library/jest-dom`](https://github.com/testing-library/jest-dom#readme) - Custom jest matchers to test the state of the DOM
- [`@testing-library/react`](https://testing-library.com/docs/react-testing-library/intro/) - Simple and complete React DOM testing utilities
- [`@testing-library/user-event`](https://github.com/testing-library/user-event#readme) - Fire events the same way the user does


### Eslint
- [`eslint-config-prettier`](https://www.npmjs.com/package/eslint-config-prettier) - Turns off all rules that are unnecessary or might conflict with Prettier.
- [`eslint-import-resolver-alias`](https://www.npmjs.com/package/eslint-import-resolver-alias) - a simple Node behavior import resolution plugin for eslint-plugin-import, supporting module alias.
- [`eslint-plugin-babel`](https://www.npmjs.com/package/eslint-plugin-babel) - an eslint rule plugin companion to babel-eslint.
- [`eslint-plugin-import`](https://www.npmjs.com/package/eslint-plugin-import) - This plugin intends to support linting of ES2015+ (ES6+) import/export syntax, and prevent issues with misspelling of file paths and import names.
- [`eslint-plugin-prettier`](https://www.npmjs.com/package/eslint-plugin-prettier) - Runs prettier as an eslint rule.
- [`eslint-plugin-react`](https://www.npmjs.com/package/eslint-plugin-react) - React specific linting rules for ESLint.
- [`eslint-plugin-react-hooks`](https://github.com/facebook/react/tree/master/packages/eslint-plugin-react-hooks) - Enforces the Rules of React Hooks.


### Plugins

- [`clean-webpack-plugin`](https://github.com/johnagan/clean-webpack-plugin) - Remove/clean build folders
- [`copy-webpack-plugin`](https://github.com/webpack-contrib/copy-webpack-plugin) - Copy files to build directory
- [`html-webpack-plugin`](https://github.com/jantimon/html-webpack-plugin) - Generate HTML files from template
- [`mini-css-extract-plugin`](https://github.com/webpack-contrib/mini-css-extract-plugin) - Extract CSS into separate files
- [`optimize-css-assets-webpack-plugin`](https://github.com/NMFR/optimize-css-assets-webpack-plugin) - Optimize and minimize CSS assets
- [`react-refresh-webpack-plugin`](https://github.com/pmmmwh/react-refresh-webpack-plugin) - HMR using React Fast Refresh
- [`@svgr/webpack`](https://github.com/mrsteele/dotenv-webpack) - SVGR can be used as a webpack loader, this way you can import your SVG directly as a React Component.
- [`babel-jest`](https://www.npmjs.com/package/babel-jest) - Babel jest plugin for transforimg ```.js``` and ```.jsx``` files

### fetch
- [`react-fetch-hook`](https://github.com/ilyalesik/react-fetch-hook) Fetch static content eg. labels, text, description
- [`graphql-hook`](https://github.com/nearform/graphql-hooks) Fetch data from graphql api



