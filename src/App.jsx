import React from 'react'
import './styles/App.scss'
import FormValidation from './components/organisms/FormValidation/FormValidation'
const App = () => {
  return <FormValidation />
}
export default App
