import { render } from '@testing-library/react'
import '@testing-library/jest-dom'

import { ButtonFunction } from './index'

describe('<ButtonFunction />', () => {
  test('should render correctly', () => {
    const { getByText } = render(<ButtonFunction>Test</ButtonFunction>)
    expect(getByText(/Test/i)).toBeInTheDocument()
  })
})
