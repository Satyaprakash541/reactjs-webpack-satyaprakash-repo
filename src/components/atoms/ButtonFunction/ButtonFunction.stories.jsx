import { ButtonFunction } from './index'

export default {
  title: 'ButtonFunction',
  component: ButtonFunction,
  argTypes: {
    className: ''
  }
}

const Template = (args) => (
  <ButtonFunction {...args}>sample component</ButtonFunction>
)

export const Primary = Template.bind({})
Primary.args = {
  primary: true,
  label: 'ButtonFunction'
}
