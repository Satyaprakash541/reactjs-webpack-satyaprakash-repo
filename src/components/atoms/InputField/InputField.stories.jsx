import { InputField } from './index'

export default {
  title: 'InputField',
  component: InputField,
  argTypes: {
    className: ''
  }
}

const Template = (args) => <InputField {...args}>sample component</InputField>

export const Primary = Template.bind({})
Primary.args = {
  primary: true,
  label: 'InputField'
}
