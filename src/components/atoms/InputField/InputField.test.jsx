import { render } from '@testing-library/react'
import '@testing-library/jest-dom'

import { InputField } from './index'

describe('<InputField />', () => {
  test('should render correctly', () => {
    const { getByText } = render(<InputField>Test</InputField>)
    expect(getByText(/Test/i)).toBeInTheDocument()
  })
})
