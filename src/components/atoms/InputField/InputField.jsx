import React from 'react'
import PropTypes from 'prop-types'

const InputField = ({ className, type, value, onChange }) => (
  <input className={className} type={type} value={value} onChange={onChange} />
)

InputField.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.any,
  onChange: PropTypes.func
}

export default InputField
