import React from 'react'
import { Heading } from './Heading'
import './styles.scss'

export default {
  title: 'Components/Heading',
  component: Heading,
  argTypes: {
    lavel: 'H1',
    children: {
      control: { type: 'text' }
    }
  }
}

const Template = (args) => <Heading {...args} />

export const Default = Template.bind({})
Default.args = {
  children: 'Default Heading',
  lavel: 'H1'
}

export const CustomHeading = Template.bind({})
CustomHeading.args = {
  lavel: 'h2',
  children: 'Custom Heading'
}
