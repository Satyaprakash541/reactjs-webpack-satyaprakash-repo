import React from 'react'
import PropTypes from 'prop-types'

const FormButton = ({ className, children, onClick }) => (
  <button className={className} onClick={onClick}>
    {children}
  </button>
)

FormButton.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func
}

export default FormButton
