import { render } from '@testing-library/react'
import '@testing-library/jest-dom'

import { FormButton } from './index'

describe('<FormButton />', () => {
  test('should render correctly', () => {
    const { getByText } = render(<FormButton>Test</FormButton>)
    expect(getByText(/Test/i)).toBeInTheDocument()
  })
})
