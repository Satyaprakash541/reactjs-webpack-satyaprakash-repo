import { FormButton } from './index'

export default {
  title: 'FormButton',
  component: FormButton,
  argTypes: {
    className: ''
  }
}

const Template = (args) => <FormButton {...args}>sample component</FormButton>

export const Primary = Template.bind({})
Primary.args = {
  primary: true,
  label: 'FormButton'
}
