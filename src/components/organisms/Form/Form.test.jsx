import { render } from '@testing-library/react'
import '@testing-library/jest-dom'

import { Form } from './index'

describe('<Form />', () => {
  test('should render correctly', () => {
    const { getByText } = render(<Form>Test</Form>)
    expect(getByText(/Test/i)).toBeInTheDocument()
  })
})
