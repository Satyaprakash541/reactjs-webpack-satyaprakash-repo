import { Form } from './index'

export default {
  title: 'Form',
  component: Form,
  argTypes: {
    className: ''
  }
}

const Template = (args) => <Form {...args}>sample component</Form>

export const Primary = Template.bind({})
Primary.args = {
  primary: true,
  label: 'Form'
}
