import { React, useState } from 'react'
import PropTypes from 'prop-types'
import FormField from '../../molecules/FormField/FormField'

const Form = ({ onSubmit }) => {
  const [formData, setFormData] = useState({
    username: '',
    password: '',
    email: ''
  })

  const handleSubmit = (e) => {
    e.preventDefault()
    onSubmit(formData)
  }

  const handleInputChange = (fieldName, value) => {
    setFormData({ ...formData, [fieldName]: value })
  }

  return (
    <form onSubmit={handleSubmit}>
      <FormField
        label='Username'
        type='text'
        value={formData.username}
        onChange={(e) => handleInputChange('username', e.target.value)}
      />
      <FormField
        label='Password'
        type='password'
        value={formData.password}
        onChange={(e) => handleInputChange('password', e.target.value)}
      />
      <FormField
        label='Email'
        type='email'
        value={formData.email}
        onChange={(e) => handleInputChange('email', e.target.value)}
      />
      <button type='submit'>Submit</button>
    </form>
  )
}

Form.propTypes = {
  onSubmit: PropTypes.func
}
export default Form
