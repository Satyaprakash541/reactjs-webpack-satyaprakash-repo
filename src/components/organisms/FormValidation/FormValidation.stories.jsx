import { FormValidation } from './index'

export default {
  title: 'FormValidation',
  component: FormValidation,
  argTypes: {
    className: ''
  }
}

const Template = (args) => (
  <FormValidation {...args}>sample component</FormValidation>
)

export const Primary = Template.bind({})
Primary.args = {
  primary: true,
  label: 'FormValidation'
}
