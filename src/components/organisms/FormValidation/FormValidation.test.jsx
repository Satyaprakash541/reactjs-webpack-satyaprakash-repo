import { render } from '@testing-library/react'
import '@testing-library/jest-dom'

import { FormValidation } from './index'

describe('<FormValidation />', () => {
  test('should render correctly', () => {
    const { getByText } = render(<FormValidation>Test</FormValidation>)
    expect(getByText(/Test/i)).toBeInTheDocument()
  })
})
