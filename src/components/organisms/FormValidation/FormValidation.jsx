import React from 'react'
import Form from '../Form/Form'

const FormValidation = () => {
  const handleSubmit = (formData) => {
    if (validateForm(formData)) {
      console.log('Form submitted with data:', formData)
    } else {
      console.error('Form validation failed')
    }
  }

  const validateForm = (formData) => {
    const errors = {}

    if (!formData.username.trim()) {
      errors.username = 'Username is required'
    }
    if (!validatePassword(formData.password)) {
      errors.password =
        'Password must be between 6 and 15 characters, contain at least one lowercase letter, one uppercase letter, one digit, and one special character.'
    }
    if (!validateEmail(formData.email)) {
      errors.email = 'Invalid email format'
    }

    if (Object.keys(errors).length === 0) {
      return true
    } else {
      console.error('Validation errors:', errors)
      return false
    }
  }

  const validateEmail = (email) => /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)

  const validatePassword = (password) =>
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,15}$/.test(
      password
    )

  return (
    <div>
      <h1>Form Validation</h1>
      <Form onSubmit={handleSubmit} />
    </div>
  )
}

export default FormValidation
