import { render } from '@testing-library/react'
import '@testing-library/jest-dom'

import { FormField } from './index'

describe('<FormField />', () => {
  test('should render correctly', () => {
    const { getByText } = render(<FormField>Test</FormField>)
    expect(getByText(/Test/i)).toBeInTheDocument()
  })
})
