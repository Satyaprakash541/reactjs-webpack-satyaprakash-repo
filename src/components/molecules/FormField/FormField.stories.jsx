import { FormField } from './index'

export default {
  title: 'FormField',
  component: FormField,
  argTypes: {
    className: ''
  }
}

const Template = (args) => <FormField {...args}>sample component</FormField>

export const Primary = Template.bind({})
Primary.args = {
  primary: true,
  label: 'FormField'
}
