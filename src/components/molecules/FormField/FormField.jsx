import React from 'react'
import PropTypes from 'prop-types'
import InputField from '../../atoms/InputField/InputField'

const FormField = ({ label, type, value, onChange, error }) => (
  <div>
    <label>{label}</label>
    <InputField type={type} value={value} onChange={onChange} />
    {error && <span className='error-message'>{error}</span>}
  </div>
)

FormField.propTypes = {
  label: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.any,
  onChange: PropTypes.func,
  error: PropTypes.string
}

export default FormField
